const reducer = (state = [], action) => {
    switch (action.type) {
        case 'ADD_TODOS' :
            return [...state, {
                topic: action.topic,
                completed: false
            }]
        case 'TOGGLE_TODOS':
            return state.map((each, index) => {
                if (index === action.targetIndex) {
                    return {
                        ...each,
                        completed: !each.completed
                    }
                }
                return each
            })
        case 'REMOVE_TODOS' :
            return state.filter((each, index) => {
                return index !== action.targetIndex
            })
    }
}
 
const createStore = reducer => {
    let state
   
    getState = () => state
 
    dispatch = action => {
        state = reducer(state, action)
    }
 
    dispatch({ type: 'INIT' })
 
    return {
        getState,
        dispatch
    }
}
 
const store = createStore(reducer)
 
store.dispatch({
    type: 'ADD_TODOS',
    topic: 'เอาผ้าไปซัก'
})
 
console.log(store.getState())
// Result
// [
//     {
//         topic: 'เอาผ้าไปซัก',
//         completed: false
//     }
// ]
 
store.dispatch({
    type: 'ADD_TODOS',
    topic: 'รดน้ำต้นไม้'
})
 
console.log(store.getState())
 
// Result
// [
//     {
//         topic: 'เอาผ้าไปซัก',
//         completed: false
//     },
//     {
//         topic: 'รดน้ำต้นไม้',
//         completed: false
//     }
// ]
 
store.dispatch({
    type: 'ADD_TODOS',
    topic: 'ซื้อพิซซ่า'
})
 
console.log(store.getState())
 
// Result
// [
//     {
//         topic: 'เอาผ้าไปซัก',
//         completed: false
//     },
//     {
//         topic: 'รดน้ำต้นไม้',
//         completed: false
//     }
//     {
//         topic: 'ซื้อพิซซ่า',
//         completed: false
//     }
// ]
 
store.dispatch({
    type: 'TOGGLE_TODOS',
    targetIndex: 1
})
 
console.log(store.getState())
 
 
// Result
// [
//     {
//         topic: 'เอาผ้าไปซัก',
//         completed: false
//     },
//     {
//         topic: 'รดน้ำต้นไม้',
//         completed: true
//     }
//     {
//         topic: 'ซื้อพิซซ่า',
//         completed: false
//     }
// ]
 
store.dispatch({
    type: 'TOGGLE_TODOS',
    targetIndex: 1
})
 
console.log(store.getState())
 
 
// Result
// [
//     {
//         topic: 'เอาผ้าไปซัก',
//         completed: false
//     },
//     {
//         topic: 'รดน้ำต้นไม้',
//         completed: false
//     }
//     {
//         topic: 'ซื้อพิซซ่า',
//         completed: false
//     }
// ]
 
store.dispatch({
    type: 'REMOVE_TODOS',
    targetIndex: 1
})
 
console.log(store.getState())
 
 
// Result
// [
//     {
//         topic: 'เอาผ้าไปซัก',
//         completed: false
//     },
//     {
//         topic: 'ซื้อพิซซ่า',
//         completed: false
//     }
// ]