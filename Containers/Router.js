import React, { Component } from 'react';
import { Route, NativeRouter, Switch, Redirect } from 'react-router-native'
import Login from './Login'
import Profile from './Profile'
import ProfileEdit from './ProfileEdit'
import ProductList from './ProductList'
import ProductDetail from './ProductDetail'
import ProductAdd from './ProductAdd'
import ProductEdit from './ProductEdit'

class Router extends Component {
    render() {
        return (
            <NativeRouter>
                <Switch>
                    <Route exact path='/' component={Login} />
                    <Route exact path='/profile' component={Profile} />
                    <Route exact path='/profile_edit' component={ProfileEdit} />
                    <Route exact path='/product_list' component={ProductList} />
                    <Route exact path='/product_detail' component={ProductDetail} />
                    <Route exact path='/product_add' component={ProductAdd} />
                    <Route exact path='/product_edit' component={ProductEdit} />
                    <Redirect to='/product_list' />
                </Switch>
            </NativeRouter>
        )
    }
}

export default Router