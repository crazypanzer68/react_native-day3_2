import React, { Component } from 'react'
import { StyleSheet, View, Image, Modal, TouchableOpacity, ScrollView } from 'react-native'
import { Button } from '@ant-design/react-native'
import styled from 'styled-components'

class ProductList extends Component {
    state = {
        images: '',
        isShowModal: false
    }
    showModal(visible) {
        this.setState({ isShowModal: visible })
    }
    showImage(image) {
        this.setState({ images: image })
    }
    goToprofile = () => {
        this.props.history.push('/profile')
    }
    goToproductadd = () => {
        this.props.history.push('/product_add')
    }
    goToproductedit = () => {
        this.props.history.push('/product_edit')
    }

    render() {
        return (
            <View style={styles.container}>
                <Button style={{ color: '#311b92', backgroundColor: '#689f38' }}
                    onPress={this.goToprofile}>PROFILE</Button>
                <View style={[styles.row]}>
                    <View style={[styles.column]}>
                        <View style={[styles.pictureContainer]}>
                            <Modal
                                transparent={true}
                                visible={this.state.isShowModal}
                            >
                                <View style={[styles.row]}>
                                    <View style={[styles.modal]}>
                                        <Button style={{ width: '40%', color: '#311b92', backgroundColor: '#689f38' }}
                                            onPress={() => { this.showModal(!this.state.isShowModal) }}>{'X'}</Button>
                                        <Center>
                                            <Image source={{ uri: this.state.images }}
                                                style={{ width: 180, height: 180 }} borderRadius={50}></Image>
                                        </Center>
                                        <ShowText>Product Name : Untitle</ShowText>
                                        <ShowText>Product Detail : NOPE</ShowText>
                                        <Button style={{ alignContent: 'center', color: '#311b92', backgroundColor: '#689f38' }}
                                            onPress={this.goToproductedit}>EDIT PRODUCT</Button>
                                    </View>
                                </View>
                            </Modal>
                            <TouchableOpacity
                                onPress={() => {
                                    this.showModal(true)
                                    this.showImage('https://images.pexels.com/photos/104827/cat-pet-animal-domestic-104827.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260')
                                }}
                            >
                                <Image source={{ uri: 'https://images.pexels.com/photos/104827/cat-pet-animal-domestic-104827.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260' }}
                                    style={styles.img}></Image>
                            </TouchableOpacity>
                        </View>
                        <View style={[styles.pictureContainer]}>
                            <TouchableOpacity
                                onPress={() => {
                                    this.showModal(true)
                                    this.showImage('https://images.pexels.com/photos/104827/cat-pet-animal-domestic-104827.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260')
                                }}
                            >
                                <Image source={{ uri: 'https://images.pexels.com/photos/104827/cat-pet-animal-domestic-104827.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260' }}
                                    style={styles.img}></Image>
                            </TouchableOpacity>
                        </View>
                        <View style={[styles.pictureContainer]}>
                            <TouchableOpacity
                                onPress={() => {
                                    this.showModal(true)
                                    this.showImage('https://images.pexels.com/photos/104827/cat-pet-animal-domestic-104827.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260')
                                }}
                            >
                                <Image source={{ uri: 'https://images.pexels.com/photos/104827/cat-pet-animal-domestic-104827.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260' }}
                                    style={styles.img}></Image>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={[styles.column]}>
                        <View style={[styles.pictureContainer]}>
                            <TouchableOpacity
                                onPress={() => {
                                    this.showModal(true)
                                    this.showImage('https://images.pexels.com/photos/104827/cat-pet-animal-domestic-104827.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260')
                                }}
                            >
                                <Image source={{ uri: 'https://images.pexels.com/photos/104827/cat-pet-animal-domestic-104827.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260' }}
                                    style={styles.img}></Image>
                            </TouchableOpacity>
                        </View>
                        <View style={[styles.pictureContainer]}>
                            <TouchableOpacity
                                onPress={() => {
                                    this.showModal(true)
                                    this.showImage('https://images.pexels.com/photos/104827/cat-pet-animal-domestic-104827.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260')
                                }}
                            >
                                <Image source={{ uri: 'https://images.pexels.com/photos/104827/cat-pet-animal-domestic-104827.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260' }}
                                    style={styles.img}></Image>
                            </TouchableOpacity>
                        </View>
                        <View style={[styles.pictureContainer]}>
                            <TouchableOpacity
                                onPress={() => {
                                    this.showModal(true)
                                    this.showImage('https://images.pexels.com/photos/104827/cat-pet-animal-domestic-104827.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260')
                                }}
                            >
                                <Image source={{ uri: 'https://images.pexels.com/photos/104827/cat-pet-animal-domestic-104827.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260' }}
                                    style={styles.img}></Image>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <Button style={{ alignContent: 'center', marginTop: '5%', color: '#311b92', backgroundColor: '#689f38' }}
                 onPress={this.goToproductadd}>ADD PRODUCT</Button>
            </View>
        )
    }
}
export default ProductList

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#C1E1C5',
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    row: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    column: {
        flex: 1,
        flexDirection: 'column',
        margin: 10,
    },
    text: {
        textAlignVertical: 'center',
        textAlign: 'center',
        fontSize: 25
    },
    pictureContainer: {
        width: 150,
        height: 150,
        marginBottom: 50,
        marginLeft: 15
    },
    img: {
        borderRadius: 10,
        width: '100%', height: '100%',
        alignItems: 'center'

    },
    modal: {
        marginTop: 30,
        width: '80%',
        height: '80%',
        backgroundColor: '#00D084'
    },
    imgModal: {
        flex: 1,
    }
})

const Center = styled.View`
    alignContent: center;
    alignItems: center;
    margin-top: 5%;
    margin-bottom: 5%;
`
const ShowText = styled.Text`
    fontSize: 25;
    color: #7b1fa2;
    margin-left: 5%;
    margin-right: 5%;
    margin-bottom: 1%;
`