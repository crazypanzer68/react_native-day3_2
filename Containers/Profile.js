import React, { Component } from 'react';
import { View, Image } from 'react-native';
import { Button } from '@ant-design/react-native'
import styled from 'styled-components'

class Profile extends Component {
    state = {
        value: '',
        value1: '',
    }
    UNSAFE_componentWillMount() {
        console.log(this.props)
    }
    goToproductlist = () => {
        this.props.history.push('/product_list')
    }
    goToprofileedit = () => {
        this.props.history.push('/profile_edit')
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: '#C1E1C5' }}>
                <View style={{ flexDirection: 'row', marginTop: '5%', marginLeft: '5%', marginRight: '5%', backgroundColor: '#00D084' }}>
                    <Button style={{ width: '40%', color: '#311b92', backgroundColor: '#689f38' }} onPress={this.goToproductlist}>{'<'}</Button>
                    <TextHead>Profile</TextHead>
                </View>
                <View style={{ marginTop: '5%', marginLeft: '5%', marginRight: '5%', backgroundColor: '#7BDCB5' }}>
                    <Center>
                        <Image
                            style={{ width: 180, height: 180 }} borderRadius={50}
                            source={{ uri: 'https://amp.businessinsider.com/images/5654150584307663008b4ed8-750-563.jpg' }}
                        />
                    </Center>
                    <ShowText>Username : </ShowText>
                    <ShowText>First Name :</ShowText>
                    <ShowText>Last Name :</ShowText>
                    <Button style={{ alignContent: 'center', marginTop: '5%', color: '#311b92', backgroundColor: '#689f38' }}
                        onPress={this.goToprofileedit}>EDIT PROFILE</Button>
                </View>
            </View>
        )
    }
}

export default Profile

const ShowText = styled.Text`
    fontSize: 25;
    color: #7b1fa2;
    margin-left: 5%;
    margin-right: 5%;
    margin-bottom: 1%;
`
const TextHead = styled.Text`
    fontSize: 30;
    fontWeight: bold;
    color: #4a148c;
    margin-left: auto;
    margin-right: 5%;
    backgr
`
const Center = styled.View`
    alignContent: center;
    alignItems: center;
    margin-top: 5%;
    margin-bottom: 5%;
`