import React, { Component } from 'react';
import { View, Image } from 'react-native';
import { Button, InputItem } from '@ant-design/react-native'
import styled from 'styled-components'

class ProfileEdit extends Component {
    state = {
        value: '',
        value1: '',
    }
    UNSAFE_componentWillMount() {
        console.log(this.props)
    }
    goToprofile = () => {
        this.props.history.push('/profile')
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: '#C1E1C5' }}>
                <View style={{ flexDirection: 'row', marginTop: '5%', marginLeft: '5%', marginRight: '5%', backgroundColor: '#00D084' }}>
                    <Button style={{ width: '40%', color: '#311b92', backgroundColor: '#689f38' }} onPress={this.goToprofile}>{'<'}</Button>
                    <TextHead>Edit Profile</TextHead>
                </View>
                <View style={{ marginTop: '5%', marginLeft: '5%', marginRight: '5%', backgroundColor: '#7BDCB5' }}>
                    <Center>
                        <Image
                            style={{ width: 180, height: 180 }} borderRadius={50}
                            source={{ uri: 'https://amp.businessinsider.com/images/5654150584307663008b4ed8-750-563.jpg' }}
                        />
                    </Center>
                    <ShowText>Username : </ShowText>
                    <ShowText>First Name</ShowText>
                    <InputItem
                        clear
                        value={this.state.value}
                        onChange={value => {
                            this.setState({
                                value,
                            });
                        }}
                        placeholder="Your first name"
                    ></InputItem>
                    <ShowText>Last Name</ShowText>
                    <InputItem
                        clear
                        value={this.state.value1}
                        onChange={value1 => {
                            this.setState({
                                value1,
                            });
                        }}
                        placeholder="Your last name"
                    ></InputItem>
                    <Button style={{ alignContent: 'center', marginTop: '5%', color: '#311b92', backgroundColor: '#689f38' }}
                        onPress={this.goToprofile} >SAVE</Button>
                </View>
            </View>
        )
    }
}

export default ProfileEdit

const ShowText = styled.Text`
    fontSize: 25;
    color: #7b1fa2;
    margin-left: 5%;
    margin-right: 5%;
    margin-bottom: 1%;
`
const TextHead = styled.Text`
    fontSize: 30;
    fontWeight: bold;
    color: #4a148c;
    margin-left: auto;
    margin-right: 5%;
    backgr
`
const Center = styled.View`
    alignContent: center;
    alignItems: center;
    margin-top: 5%;
    margin-bottom: 5%;
`